import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import Components from "unplugin-vue-components/vite";
import { prismjsPlugin } from "vite-plugin-prismjs";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    Components({}),
    prismjsPlugin({
      languages: ["javascript", "log"],
      plugins: [],
      theme: "twilight",
      css: true,
    }),
  ],
  server: {
    proxy: {
      "/api": {
        target: "http://owc-cluster-gamma-00:31958",
      },
    },
  },
  build: {
    rollupOptions: {
      // https://rollupjs.org/guide/en/#outputmanualchunks
      output: {
        manualChunks: {
          "group-user": [
            "./src/views/ExecutionsView.vue",
            "./src/views/JobLogs.vue",
            "./src/views/JobsView.vue",
            "./src/views/JobSubmission.vue",
          ],
        },
      },
    },
  },
});
