import { createRouter, createWebHistory } from "vue-router";
import JobsTableVue from "./views/JobsView.vue";
import ExecutionsTableVue from "./views/ExecutionsView.vue";
import JobLogsVue from "./views/JobLogs.vue";
import JobSubmissionVue from "./views/JobSubmission.vue";
import NodesViewVue from "./views/NodesView.vue";
import ModulesViewVue from "./views/ModulesView.vue";

const routes = [
  {
    name: "home",
    path: "/",
    redirect: { name: "jobs" },
  },
  {
    name: "submit",
    path: "/submit",
    component: JobSubmissionVue,
    props: true,
  },
  {
    name: "jobs",
    path: "/jobs",
    component: JobsTableVue,
  },
  {
    name: "executions",
    path: "/jobs/:id",
    component: ExecutionsTableVue,
    props: true,
  },
  {
    name: "logs_redirect",
    path: "/jobs/:id/logs",
    redirect: (to: any) => ({
      name: "executions",
      params: { id: to.params.id },
    }),
  },
  {
    name: "logs",
    path: "/jobs/:id/logs/:executionId",
    component: JobLogsVue,
    props: true,
  },
  {
    name: "nodes",
    path: "/nodes",
    component: NodesViewVue,
  },
  {
    name: "modules",
    path: "/modules",
    component: ModulesViewVue,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
