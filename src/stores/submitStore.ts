import { defineStore } from "pinia";

export const useSubmitStore = defineStore("submit", {
  state: () => {
    return {
      jobName: "",
      jobDescription: "",
      processPath: "",
      mountPath: "",
      environmentId: "",
      nodeId: "",
      nodeCpu: "",
      nodeMemory: "",
      modules: {} as Record<string, string>[],
      mounts: [] as {
        mountPath: string,
        remote: string,
        password: string,
        username: string,
        branch: string,
        no_history: boolean,
        clone_submodules: boolean,
        lfs_threshold: number,
        errors: string[],
      }[]
    };
  },
  actions: {
    setFromJobObject(payload: any) {
      this.jobName = payload.name;
      this.jobDescription = payload.description;
      this.processPath = payload.configuration.processPath;
      this.environmentId = payload.configuration.environmentId ?? "";
      this.nodeId = payload.configuration.context.nodeId ?? "";
      this.nodeCpu = payload.configuration.context.resources?.cpu ?? "";
      this.nodeMemory = payload.configuration.context.resources?.memory ?? "";
      this.modules = payload.configuration.context.modules ?? {};

      payload.configuration.context.mounts.forEach((mount: any) => {
        this.mounts.push({ remote: mount.options.remote, username: mount.options["auth.username"], password: mount.options["auth.password"], branch: mount.options.branch, mountPath: mount.path, no_history: mount.options.no_history, clone_submodules: mount.options.clone_submodules, lfs_threshold: mount.options.lfs_threshold, errors: [] })
      })
    },
  },
});
