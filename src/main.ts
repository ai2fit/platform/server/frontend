import Vue, { createApp } from "vue";
import "./style.css";
import App from "./App.vue";

import Vue3EasyDataTable from "vue3-easy-data-table";
import "vue3-easy-data-table/dist/style.css";
import SmartTable from "vuejs-smart-table";
import vueProgress from "vue-ellipse-progress";
import router from "./router";
import { createPinia } from "pinia";

const app = createApp(App);
app.use(router);
app.use(createPinia());
app.component("EasyDataTable", Vue3EasyDataTable);
app.use(SmartTable);
app.use(vueProgress);
app.mount("#app");
